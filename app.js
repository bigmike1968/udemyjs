/*******************
*Variables and data types
*/
/*

var firstName = "Mike";
console.log(firstName);

var lastName = "Dodson";
var age = 50;

var fullAge = true;
console.log(fullAge);

var job;
console.log(job);

job = "Web Developer";
console.log(job);

 
/*******************
*Variables mutation and type coercion
*/
/*

var firstName = "Mike";
var lastName = "Dodson";
var age = 50;

console.log(firstName + " is " + age + " years old.");

var job, isMarried;
job = "Web Developer";
isMarried = true;

console.log(firstName + " " + lastName + " is a " + age + " year old  " + job + ". Is he married? " + isMarried);

// Variable mutation

age = 24;
job = "NFL Wide Reciever";
isMarried = false;

alert(firstName + " " + lastName + " is a " + age + " year old  " + job + ". Is he married? " + isMarried);

var lastName = prompt("What is your last name.");
console.log(firstName + " " + lastName);


/*******************
*Basic Operators
*/
/*

var now, yearMike, yearMarie;
now = 2019;
ageMike = 51;
ageMarie = 41;

// Math Operators
yearMike = now - ageMike;
yearMarie = now - ageMarie;

console.log(yearMarie);
console.log(yearMike);
console.log(now * 2);
console.log(now * 2 / 2);

// Logical Operators
var mikeOlder =  ageMike > ageMarie;
console.log(mikeOlder);


// Typeof Operator
console.log(typeof now);
console.log(typeof mikeOlder);
console.log(typeof "Mike loves super heroes");
var z;
console.log(typeof z);


/*******************
*Operator Precedence
*/
/*

var now =  2019;
var yearMike = 1968;
var fullAge = 21;

// Multiple operators
var isFullAge = now - yearMike >= fullAge; //true
console.log(isFullAge);

// Grouping
var ageMike = now - yearMike;
var ageMarie = 41;
var avg = (ageMike + ageMarie) / 2;
console.log(avg);

// Multiple assignments
var x, y;
x = y = (10 + 15) * 3 - 25; // 25 * 3 - 25 // 75 - 25 // 50
console.log(x, y);

// More operators
x *= 4;
console.log(x);

x += 15;
console.log(x);

x ++;
console.log(x);


/*******************
*Coding Challenge 1
*/

/*
Mike and Marie are trying to compare their BMI (Body Mass Index),
which is calculated using the formula: 
BMI = mass / height * 2 = mass / (height * height).
(mass in kg and height in meter).

1. store Mike and Marie's mass and height in variables.

2. Calculate both their BMI's.

3. Create a boolean variable containing information about
whether Mark has a higher BMI than Marie.

4. Print a string to the console containing the variable
from step 3. (I.E. "Does Mike have a greater BMI than Marie?")
*/

var mikeMass, marieMass, mikeHeight, marieHeight, mikeBMI, marieBMI, personOne, personTwo, higherBMI;

mikeMass = 230 * 0.45; //kg
console.log(mikeMass);

marieMass = 280 * 0.45; //kg
console.log(marieMass);

mikeHeight = 76 * 0.025; //meters
console.log(mikeHeight);

marieHeight = 70 * 0.025; //meters
console.log(marieHeight);

mikeBMI = mikeMass / (mikeHeight * 2);
console.log(mikeBMI);

marieBMI = marieMass / (marieHeight * 2);
console.log(marieBMI);

personOne = "Mike";
personTwo = "Marie";

higherBMI = mikeBMI >= marieBMI;
console.log(personOne + " has a greater BMI than " + personTwo + ". " + higherBMI);













































